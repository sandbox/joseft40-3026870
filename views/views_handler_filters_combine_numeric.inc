<?php

/**
 * @file
 * Definition of views_handler_filter_combine_numeric.
 */
class views_handler_filters_combine_numeric extends views_handler_filter_numeric {

  /**
   * @var views_plugin_query_default
   */
  public $query;

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['fields'] = array('default' => array());

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $this->view->init_style();

    // Allow to choose all fields as possible.
    if ($this->view->style_plugin->uses_fields()) {
      $options = array();
      foreach ($this->view->display_handler->get_handlers('field') as $name => $field) {
        $options[$name] = $field->ui_name(TRUE);
      }
      if ($options) {
        $form['fields'] = array(
          '#type' => 'select',
          '#title' => t('Choose fields to combine for filtering'),
          '#description' => t("This filter doesn't work for very special field handlers."),
          '#multiple' => TRUE,
          '#options' => $options,
          '#default_value' => $this->options['fields'],
        );
      }
      else {
        form_set_error('', t('You have to add some fields to be able to use this filter.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->view->_build('field');
    $fields = array();
    // Only add the fields if they have a proper field and table alias.
    foreach ($this->options['fields'] as $id) {
      // Field access checks may have removed this handler.
      if (!isset($this->view->field[$id])) {
        continue;
      }

      $field = $this->view->field[$id];
      // Always add the table of the selected fields to be sure a table alias
      // exists.

      $field->ensure_my_table();

      if (!empty($field->field_alias) && !empty($field->field_alias)) {
        $fields[] = "$field->table_alias.$field->real_field";
      }
    }

    if ($fields) {
      $count = count($fields);
      $separated_fields = array();
      foreach ($fields as $key => $field) {
        $separated_fields[] = $field;
        if ($key < $count - 1) {
          $separated_fields[] = "' '";
        }
      }

      $expression = implode(', ', $separated_fields);
      $expression = "CONCAT_WS(' ', $expression)";

      $info = $this->operators();
      if (!empty($info[$this->operator]['method'])) {
        $this->{$info[$this->operator]['method']}($expression);
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function op_between($field) {
    $operator = 'between';
    $min = $this->value['min'];
    $max = $this->value['max'];

    if ($this->operator == $operator) {
      // Don't filter on empty strings.
      if (empty($this->value['min'])) {
        return;
      }
      $this->query->add_where_expression($this->options['group'], "$field $operator $min AND $max");

    }
    else {
      $this->query->add_where_expression($this->options['group'], "($field <= $min) OR ($field >= $max)");
    }
  }

  function op_simple($field) {
    $value = $this->value['value'];

    // Don't filter on empty strings.
    if (empty($value)) {
      return;
    }
    $this->query->add_where_expression($this->options['group'], "$field $this->operator $value");
  }
}
