<?php

/**
 * Implementation of hook_views_data().
 */
function views_filters_combine_numeric_views_data() {
  $data['views']['views_filters_combine_numeric'] = array(
    'title' => t('Combine fields filter numeric'),
    'help' => t('Combine multiple fields numeric for search.'),
    'filter' => array(
      'handler' => 'views_handler_filters_combine_numeric',
    ),
  );

  return $data;
}
